﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //-------------------------------------------------------------------------

    public float speed = 15.0f;
    public float padding = 1;
    public GameObject projectile;
    public float projectileSpeed;
    public float firingRate = 0.2f;
    public float health = 250f;

    private float xMin;
    private float xMax;

    //-------------------------------------------------------------------------

    // Use this for initialization
    void Start () {

        // calcuate the distance from the starship to the camera
        float distance = transform.position.z - Camera.main.transform.position.z;

        // get the leftmost and rightmost value from the camera
        Vector3 leftMost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
        Vector3 rightMost = Camera.main.ViewportToWorldPoint(new Vector3(1,0,distance));

        // incorporate the padding to keep the entire starship on the screen
        xMin = leftMost.x + padding;
        xMax = rightMost.x - padding;

    }

    //-------------------------------------------------------------------------

    void Fire()
    {
        Vector3 offset = new Vector3(0, 1f, 0);
        GameObject beam = Instantiate(projectile, transform.position + offset, Quaternion.identity) as GameObject;
        beam.GetComponent<Rigidbody2D>().velocity = new Vector3(0, projectileSpeed, 0);
    }

    //-------------------------------------------------------------------------

    // Update is called once per frame
    void Update () {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            InvokeRepeating("Fire", 0.000001f, firingRate);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            CancelInvoke("Fire");
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            //transform.position += new Vector3(speed * Time.deltaTime,0,0);

            transform.position += Vector3.right * speed * Time.deltaTime;

        } else if (Input.GetKey(KeyCode.LeftArrow))
        {
            //transform.position += new Vector3(-speed * Time.deltaTime, 0, 0);

            transform.position += Vector3.left * speed * Time.deltaTime;
        }

        // 
        float newX = Mathf.Clamp(transform.position.x, xMin, xMax);

        transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        
	}

    //-------------------------------------------------------------------------

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // a collision has been detected
        Debug.Log(collision);

        // check to see if the object that hit the enemy has a Projectile component and log to the console if it does
        Projectile missile = collision.gameObject.GetComponent<Projectile>();

        if (missile)
        {
            Debug.Log("Hit by a projectile");

            // applies the damage to the enemy health
            health -= missile.GetDamage();

            // remove the missile from the play area
            missile.Hit();

            // removed the enemy from the play area
            if (health <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

}
