﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    //-------------------------------------------------------------------------

    public float damage = 100f;

    //-------------------------------------------------------------------------

    public float GetDamage()
    {
        return damage;
    }

    //-------------------------------------------------------------------------
    
    public void Hit()
    {
        // remove the missile from the play area
        Destroy(gameObject);
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
}
