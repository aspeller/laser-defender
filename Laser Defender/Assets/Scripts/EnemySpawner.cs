﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    //-------------------------------------------------------------------------

    public GameObject enemyPrefab;
    public float width = 10f;
    public float height = 5f;
    public float speed = 5f;

    private float xMin;
    private float xMax;
    private float padding = 1f;
    private bool movingRight = true;

    //-------------------------------------------------------------------------

	// Use this for initialization
	void Start () {
        
        foreach (Transform child in transform)
        {
            GameObject enemy = Instantiate(enemyPrefab, child.transform.position, Quaternion.identity) as GameObject;
            enemy.transform.parent = child;
        }

        // calcuate the distance from the starship to the camera
        float distance = transform.position.z - Camera.main.transform.position.z;

        // get the leftmost and rightmost value from the camera
        Vector3 leftMost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
        Vector3 rightMost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));

        // incorporate the padding to keep the entire starship on the screen
        xMin = leftMost.x + padding;
        xMax = rightMost.x - padding;
    }

    //-------------------------------------------------------------------------

    public void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(width, height, 0));
    }

    //-------------------------------------------------------------------------

    // Update is called once per frame
    void Update () {

        if (movingRight)
        {
            transform.position += new Vector3(speed * Time.deltaTime,0,0);
        } else
        {
            transform.position += new Vector3(-speed * Time.deltaTime, 0, 0);
        }

        float rightEdgeOfFormation = transform.position.x + (0.5f * width);
        float leftEdgeOfFormation = transform.position.x - (0.5f * height);

        if (leftEdgeOfFormation < xMin)
        {
            movingRight = true;
        } else if (rightEdgeOfFormation > xMax)
        {
            movingRight = false;
        }
        
    }
    //-------------------------------------------------------------------------
}
