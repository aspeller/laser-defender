﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour {

    //-------------------------------------------------------------------------

    public float health = 150f;
    public GameObject projectile;
    public float projectileSpeed = 10f;
    public float shotsPerSecond = 0.5f;

    //-------------------------------------------------------------------------

    private void Fire()
    {
        Vector3 startPos = transform.position + new Vector3(0f, -1f, 0);

        // generate the missiles
        GameObject enemyMissile = Instantiate(projectile, startPos, Quaternion.identity) as GameObject;

        // make the missiles move
        enemyMissile.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -projectileSpeed, 0);
    }

    //-------------------------------------------------------------------------

    private void Update()
    {
        float probability = Time.deltaTime * shotsPerSecond;
        if (Random.value < probability)
        {
            Fire();
        }
        
    }

    //-------------------------------------------------------------------------

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // a collision has been detected
        Debug.Log(collision);

        // check to see if the object that hit the enemy has a Projectile component and log to the console if it does
        Projectile missile = collision.gameObject.GetComponent<Projectile>();

        if (missile)
        {
            Debug.Log("Hit by a projectile");

            // applies the damage to the enemy health
            health -= missile.GetDamage();

            // remove the missile from the play area
            missile.Hit();

            // removed the enemy from the play area
            if (health <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
}
